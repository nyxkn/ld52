#!/bin/bash

# run unoconv --listener if it complains
# docs: https://wiki.openoffice.org/wiki/Documentation/DevGuide/Spreadsheets/Filter_Options#Filter_Options_for_the_CSV_Filter

# file="data.ods"

# convert_ods_to_csv="unoconv -e FilterOptions=59,34,0,1 -f csv"

# run when file changes
# echo $file | entr $run /_
# ls *.ods | entr echo /_

################
# inotify version
# https://superuser.com/questions/181517/how-to-execute-a-command-whenever-a-file-changes

# the sleep and kill contraption is so that we're caching the event
# and replacing it with a successive event for the same file
# this is because libreoffice on save ends up triggering close_write a bunch of times
# we only want to run our command after the last write has completed
# we pad sleep with zeroes to make sure we're killing this one specifically
# not sure if needed

root_dir="$(git rev-parse --show-toplevel)"
[[ "$root_dir" ]] || root_dir=".."

monitor_dir="csv"
assets_dir="$root_dir/game/assets/csv"
mkdir -p "$assets_dir"

function convert_ods_to_csv {
    ods_file="$1"
    name=$(basename -s .ods "$ods_file")
    csv_file="$name.csv"

    unoconv -o "$assets_dir/$csv_file" -e FilterOptions=59,34,0,1 -f csv "$ods_file"

    printf -v timestamp "[%s]" "$(date +%T.%3N)"
    echo "$timestamp" "$ods_file -> $csv_file"
}

# start the listener
# this requires sudo so that it doesn't interfere with letting us open
# a regular instance of libreoffice while this is running
# alternatively, you can first start libreoffice, and then start the listener
# this works without sudo
# unoserver is a rework of unoconv that supports running libreoffice simultaneously
# but at the moment it does not support csv export flags (22/04/07)
sudo unoconv --listener &

# libreoffice &
# sleep 5
# unoconv --listener &

inotifywait -e close_write -m "$monitor_dir" |
while read -r directory events filename; do
    if [[ "$filename" == *".ods" ]]; then
        pkill -f "sleep 0.200000s"
        ( sleep 0.200000s && convert_ods_to_csv "$filename" ) &
    fi
done

