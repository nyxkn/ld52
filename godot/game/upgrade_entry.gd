extends Button


var id = ""
var current_cost = 0 setget set_cost


func _ready() -> void:
	pass


func set_cost(new_cost):
	current_cost = new_cost
	$Cost.text = str(new_cost)
