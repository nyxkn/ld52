extends Node2D


# this is your main game loop
# if you are changing levels or entering menus, they should probably be added and removed from here
# rather than changing scene
# you only want to change scene to non-game things like main menu and end screen
# i.e. when you can afford to unload everything game-related

# basic formula for cost:
# cost = base_cost * multiplier^n_owned
# this is an exponential function, and multiplier sets the speed
# all the while, benefit increase is linear, e.g. always +5 on each upgrade
# common numbers for multiplier ars 1.07 and 1.15, or inbetween
# https://gamedevelopment.tutsplus.com/articles/numbers-getting-bigger-the-design-and-math-of-incremental-games--cms-24023

const Tile = preload("res://game/tile.tscn")
const UpgradeEntry = preload("res://game/upgrade_entry.tscn")
const HarvestCircle = preload("res://game/harvest_circle.tscn")
const PlantCircle = preload("res://game/plant_circle.tscn")
#var Log = preload("res://addons/nframework/autoloads/logger.gd").new().init(self)
const Pop = preload("res://game/pop.tscn")
const Confetti = preload("res://game/confetti.tscn")

enum PlantState {
#	AUTO = -1,
	EMPTY = 0,
	SEED,
	SEEDLING,
	PLANT,
	RIPE,
}

# id, name, base cost, number of levels, flavor text
const UPGRADES_FIELDS := ["name", "cost", "levels"]
# speed upgrades cannot be infinite. score upgrades can
const UPGRADES_DATA := [
	[ "autoplant", "Auto Plant", 30, 1, "Offload to peons" ],
	[ "autoplant_speed", "Faster Auto Plant", 60, 10 ],
	[ "autoharvest", "Auto Harvest", 20, 1 ],
	[ "autoharvest_speed", "Faster Auto Harvest", 40, 10 ],
	[ "plantworth", "Better Harvest Yield", 25, 7 ],
	[ "growthspeed", "Faster Plant Growth", 50, 10 ],
	[ "victory", "Buy Freedom (end game)", 1000, 1, "Are you ready to move on with life?"],
	[ "expand", "Expand Field", 5, 6 ],
]
onready var upgrades := Data.label_fields_from_array_with_id(UPGRADES_DATA, UPGRADES_FIELDS)


# core
var playtime_stopwatch: Stopwatch = Stopwatch.new()
var score: int

# state
var highlighted_tile := Vector2.INF
var plants := {}
var upgrades_enabled := {}
var terrain_size := 1
var cheats := false
var cheat_speedstart := false

# stats
var stats := {
	"total_score": 0,
	"upgrades_unlocked": 0,
	"playtime": 0,
	"mouse_clicks": 0,
	"plants_harvested": 0,
}


# game balance
const GROW_INTERVAL := 5.0
const GROW_INTERVAL_MIN := 1.5
const AUTOPLANT_INTERVAL := 2.0
const AUTOPLANT_INTERVAL_MIN := 0.1
const AUTOHARVEST_INTERVAL := 3.0
const AUTOHARVEST_INTERVAL_MIN := 0.2
const COST_MULTIPLIER := 1.2

# game balance state
var grow_interval := GROW_INTERVAL
var autoplant_interval := AUTOPLANT_INTERVAL
# autoharvest should always be slower than autoplant? or at least always different
# otherwise it looks odd
var autoharvest_interval := AUTOHARVEST_INTERVAL
var autoplant_time := 0.0
var autoplant_tile = Vector2.INF
var autoplant_tile_circle
var autoharvest_tile = Vector2.INF
var autoharvest_tile_circle
var autoharvest_time := 0.0
var highlight_size := 1


var terrain: TileMap

onready var help_text: Label = $"%HelpText"

onready var tileset: TileSet = preload("res://assets/aseprite/tileset.tres")
onready var highlight: TileMap = $Field/Highlight
onready var plants_map: TileMap = $Field/Plants

onready var upgrades_box: VBoxContainer = $"%Upgrades"


func _enter_tree() -> void:
	pass


func _ready() -> void:
	pass
	PauseScreen.allow_from(self)

	# it's good practice to start logic in its own function, so that it can be called on restart
	# _ready should only be for node initialization things
	# it's also good practice to let logic run decoupled from _ready
	# sometimes other functions wait for _ready signal to know if loading is complete
	# but if you start the logic before running _ready, it might throw things off
	call_deferred("game_setup")


func _exit_tree() -> void:
	pass


#func _input(event: InputEvent) -> void:
#	pass
##	Utils.log_event(event, "_input", name)


#func _gui_input(event: InputEvent) -> void:
#	pass
##	Utils.log_event(event, "_gui_input", name)


func _unhandled_input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_unhandled_input", name)

	if Config.debug_build and event is InputEventKey and event.is_pressed():
		if event.scancode == KEY_B:
			# actually you should also do cleanup before this
			game_setup()
		if event.scancode == KEY_K:
			get_tree().reload_current_scene()
		if event.scancode == KEY_1:
			game_over()
#		if event.scancode == KEY_T:
#			expand_terrain()
		if event.scancode == KEY_G:
			cheats = !cheats
			add_score(0)
		if event.scancode == KEY_S:
			grow_interval = 0.1
		if event.scancode == KEY_W:
			add_score(100)
#		if event.scancode == KEY_U:
#			add_score(100)


	if event.is_action_pressed("ui_select"):
		pass

	if event.is_action_pressed("click"):
		mouse_click()


#func _unhandled_key_input(event: InputEventKey) -> void:
#	pass
##	Utils.log_event(event, "_unhandled_key_input", name)


func _process(delta: float) -> void:
	var local_position = terrain.to_local(get_global_mouse_position())
	var map_position = terrain.world_to_map(local_position)
	var cell = terrain.get_cellv(map_position)
	if highlighted_tile != Vector2.INF:
		highlight.set_cellv(highlighted_tile, TileMap.INVALID_CELL)
	if cell != TileMap.INVALID_CELL:
		if ((plants[map_position].state == 0 or plants[map_position].state == PlantState.RIPE) and
				highlighted_tile != autoplant_tile and highlighted_tile != autoharvest_tile):

			highlight.set_cellv(map_position, tileset.find_tile_by_name("highlight"))
		else:
			highlight.set_cellv(map_position, tileset.find_tile_by_name("highlight2"))
		highlighted_tile = map_position
	else:
		highlighted_tile = Vector2.INF

	for pos in plants:
		if plants[pos].state > 0 and F.time() > plants[pos].time + grow_interval:
			grow_plant(pos)

	for id in upgrades_enabled:
		if id == "autoplant":
			if autoplant_tile == Vector2.INF:
				for pos in plants:
					if plants[pos].state == PlantState.EMPTY:
						autoplant_tile = pos
						break
				if autoplant_tile != Vector2.INF:
					autoplant_tile_circle = PlantCircle.instance()
					autoplant_tile_circle.position = terrain.map_to_world(autoplant_tile) + Vector2(0, 8)
					$Field/Terrains.add_child(autoplant_tile_circle)
					autoplant_time = F.time()
			if autoplant_tile != Vector2.INF and F.time() > autoplant_time + autoplant_interval:
				change_plant_state(autoplant_tile, PlantState.SEED)
				autoplant_time = F.time() + F.rng.randf_range(
					0, max(0.1, autoplant_interval / 10.0))
				autoplant_tile_circle.hide()
				autoplant_tile_circle.free()
				autoplant_tile = Vector2.INF

		if id == "autoharvest":
			if autoharvest_tile == Vector2.INF:
				for pos in plants:
					if plants[pos].state == PlantState.RIPE:
						autoharvest_tile = pos
						break
				if autoharvest_tile != Vector2.INF:
					autoharvest_tile_circle = HarvestCircle.instance()
					autoharvest_tile_circle.position = terrain.map_to_world(autoharvest_tile) + Vector2(0, 8)
					$Field/Terrains.add_child(autoharvest_tile_circle)
					autoharvest_time = F.time()
			if autoharvest_tile != Vector2.INF and F.time() > autoharvest_time + autoharvest_interval:
				harvest_plant(autoharvest_tile)
				autoharvest_time = F.time() + F.rng.randf_range(
					0, max(0.1, autoharvest_interval / 10.0))
				autoharvest_tile_circle.hide()
				autoharvest_tile_circle.free()
				autoharvest_tile = Vector2.INF

#			if F.time() > autoharvest_time + autoharvest_interval:
#				for pos in plants:
#					if plants[pos].state == PlantState.RIPE:
#						harvest_plant(pos)
#						autoharvest_time = F.time() + F.rng.randf_range(
#							0, max(0.1, autoharvest_interval / 10.0))
#						break


#func expand_terrain():
#	if terrain_size >= 6:
#		return
#
#	terrain_size += 1
#	for pos in terrain.get_used_cells():
#		set_terrain_tile(pos + Vector2(1, 0))
#		set_terrain_tile(pos + Vector2(-1, 0))
#		set_terrain_tile(pos + Vector2(0, 1))
#		set_terrain_tile(pos + Vector2(0, -1))
#
#	for pos in terrain.get_used_cells():
#		if terrain_size % 4 == 0:
#			set_terrain_tile(pos + Vector2(-1, -1))
#		elif terrain_size % 2 == 0:
#			set_terrain_tile(pos + Vector2(1, 1))
#
#	var rect = terrain.get_used_rect()
#	Log.d(rect)
#	var size = Vector2(ceil(rect.size.x * 32 / 2.0), ceil(rect.size.y * 16 / 2.0)) * $Field.scale
#	Log.d(size)
#	if size.x > 1080 or size.y > 720:
#		var f = min(720.0 / size.y, 1080.0 / size.x)
#		var scale_factor = $Field.scale.x * f
#		# the following code rounds scale_factor to the nearest power of two
#		var power = 1
#		if scale_factor >= 1:
#			while power*2 < scale_factor:
#				power *= 2
#		if scale_factor < 1:
#			while power > scale_factor:
#				power *= 0.5
#
#		if power < 4:
#			Log.w("refusing to resize terrain below scale 4")
#		else:
#			$Field.scale = Vector2(power, power)
#
#	for pos in terrain.get_used_cells():
#		init_plant(pos)



func set_terrain_tile(pos):
	terrain.set_cellv(pos, tileset.find_tile_by_name("terrain"))


func grow_plant(pos):
	if plants[pos].state < PlantState.size() - 1:
		change_plant_state(pos, plants[pos].state + 1)


func init_plant(pos):
	if not plants.has(pos):
		plants[pos] = {"state": PlantState.EMPTY, "time": 0.0}


func change_plant_state(pos, new_state):
	if plants.has(pos):
		plants[pos].state = new_state
		plants[pos].time = F.time() + F.rng.randf_range(0, max(0.1, grow_interval))
		update_plants()
	else:
		Log.w(["trying to change inexisting plant at", pos, "to", PlantState.keys()[new_state]])


func harvest_plant(pos):
	stats.plants_harvested += 1

	if stats.plants_harvested == 3:
		var tween = help_text.create_tween().set_trans(Tween.TRANS_QUAD)
		tween.tween_property(help_text, "modulate", Color(0xffffff00), 2)
		tween.tween_callback(help_text, "queue_free")

	if plants[pos].state != PlantState.RIPE:
		Log.w(["trying to harvest an unripe plant!"])
		return

	var worth = 1
	if upgrades_enabled.has("plantworth"):
		worth += upgrades_enabled["plantworth"]
	add_score(worth)

	change_plant_state(pos, PlantState.EMPTY)

	var part = Pop.instance()
	part.set_as_toplevel(true)
	part.global_position = terrain.to_global(terrain.map_to_world(pos) + Vector2(0, 8))
	terrain.add_child(part)
	part.one_shot = true
	part.emitting = true
	yield(F.wait(part.lifetime + 1.0), "completed")
	part.queue_free()



func mouse_click():
	stats.mouse_clicks += 1
	var pos = highlighted_tile
	if pos != Vector2.INF:
		if highlighted_tile == autoplant_tile or highlighted_tile == autoharvest_tile:
			Log.d("clicked on busy tile")
			return
		match plants[pos].state:
			PlantState.EMPTY:
				grow_plant(pos)
				var ap = AudioParams.new()
				ap.gain = -6
				SoundManager.play("seed", ap)
			PlantState.RIPE:
				harvest_plant(pos)
				var ap = AudioParams.new()
				ap.gain = -6
				SoundManager.play("harvest", ap)


func update_plants():
	for pos in plants:
		var state = plants[pos].state
		if state == PlantState.EMPTY:
			plants_map.set_cellv(pos, -1)
		else:
			var tile_name = PlantState.keys()[state].to_lower()
			plants_map.set_cellv(pos, tileset.find_tile_by_name(tile_name))


class Sorter:
	static func sort(a, b):
		if a.cost < b.cost:
			return true
		return false


func setup_upgrades():
	var cost_ordered = upgrades.values()
	cost_ordered.sort_custom(Sorter, "sort")

#	var skip := ["autoplant_speed", "autoharvest_speed", "victory"]
	var skip := ["autoplant_speed", "autoharvest_speed"]
#	for id in upgrades:
	for u in cost_ordered:
		var id = u.id

		if not id in skip:
			add_upgrade_entry(id)



func add_upgrade_entry(id):
	print('add upgrade')
	var entry = UpgradeEntry.instance()
	if id == "victory":
		entry.text = "?"
	else:
		entry.text = upgrades[id].name
	var level = 0
	if upgrades_enabled.has(id):
		level = upgrades_enabled[id]
	if id == "expand" or id == "plantworth":
		entry.current_cost = round(upgrades[id].cost * pow(2.5, level))
	else:
		entry.current_cost = round(upgrades[id].cost * pow(COST_MULTIPLIER, level))
	entry.id = id
	entry.disabled = true
	entry.connect("pressed", self, "upgrade_selected", [entry])

	if upgrades_box.get_child_count() == 0:
		upgrades_box.add_child(entry)
	else:
		var added = false
		for i in range(upgrades_box.get_child_count()-1, -1, -1):
			var u = upgrades_box.get_child(i)
			Log.d([entry.current_cost, u.current_cost])
			if entry.current_cost >= u.current_cost:
	#			upgrades_box.add_child_below_node(upgrades_box.get_child(i-1), entry)
				upgrades_box.add_child_below_node(u, entry)
				added = true
	#			$"%Upgrades".move_child(entry, i-1)
				break
		if not added:
			# this means its cost is lower than anything currently on. so add to front
			upgrades_box.add_child(entry)
			upgrades_box.move_child(entry, 0)

	call_deferred("refresh_upgrades")


func unlock_upgrade_ui(entry):
	var id = entry.id

	if upgrades[id].levels > upgrades_enabled[id]:
		entry.hide()
		entry.queue_free()
		yield(F.wait(0.5), "completed")
		add_upgrade_entry(id)
	else:
	#	entry.disabled = true
		entry.hide()
		entry.queue_free()


func upgrade_selected(entry):
	var id = entry.id

#	var current_level =

#	if upgrades[id]
	if upgrades_enabled.has(id):
		if upgrades[id].levels > upgrades_enabled[id]:
			upgrades_enabled[id] += 1
	else:
		upgrades_enabled[id] = 1

	if cheats:
		pay(0)
		upgrades_enabled[id] = upgrades[id].levels
	else:
		pay(entry.current_cost)



	Log.d(upgrades_enabled)

	match id:
		"victory":
			game_over()
		"growthspeed":
#			var grow_final = GROW_INTERVAL / upgrades["growthspeed"].levels
			var grow_final = GROW_INTERVAL_MIN
			var t = inverse_lerp(0, upgrades[id].levels, upgrades_enabled[id])
			grow_interval = lerp(GROW_INTERVAL, grow_final, t)
			Log.d(["new grow interval:", grow_interval])
		"autoplant_speed":
			var t = inverse_lerp(0, upgrades[id].levels, upgrades_enabled[id])
			autoplant_interval = lerp(AUTOPLANT_INTERVAL, AUTOPLANT_INTERVAL_MIN, t)
			Log.d(["new autoplant interval:", autoplant_interval])
		"autoharvest_speed":
			var t = inverse_lerp(0, upgrades[id].levels, upgrades_enabled[id])
			autoharvest_interval = lerp(AUTOHARVEST_INTERVAL, AUTOHARVEST_INTERVAL_MIN, t)
			Log.d(["new autoharvest interval:", autoharvest_interval])
		"autoplant":
			autoplant_time = F.time()
			add_upgrade_entry("autoplant_speed")
		"autoharvest":
			autoharvest_time = F.time()
			add_upgrade_entry("autoharvest_speed")
		"expand":
			expand_terrain()

	SoundManager.play("button")

	var part = Confetti.instance()
	part.set_as_toplevel(true)
	part.global_position = entry.rect_global_position + entry.rect_size / 2.0
	add_child(part)
	part.one_shot = true
	part.emitting = true

	unlock_upgrade_ui(entry)

	yield(F.wait(part.lifetime + 1.0), "completed")
	part.queue_free()




func expand_terrain():
	var terrains = $Field/Terrains
	var count = terrains.get_child_count()
#			for i in count-1:
#				if terrains.get_child(i) == terrain:
#					terrain.hide()
#					terrain = terrains.get_child(i+1)
#					terrain.show()
#					for pos in terrain.get_used_cells():
#						init_plant(pos)
#					break
	terrain.hide()
	terrain = terrains.get_child(upgrades_enabled["expand"])
	terrain.show()

#					terrain = terrains.get_child(i+1)
#					terrain.show()
	for pos in terrain.get_used_cells():
		init_plant(pos)
#					break
	var ap = AudioParams.new()
	ap.pitch_randomization = 7
	ap.gain = 3
	SoundManager.play("terrain", ap)
	yield(F.wait(0.1), "completed")
	SoundManager.play("terrain", ap)
	yield(F.wait(0.1), "completed")
	SoundManager.play("terrain", ap)
	yield(F.wait(0.1), "completed")
	SoundManager.play("terrain", ap)
	yield(F.wait(0.1), "completed")
	SoundManager.play("terrain", ap)


# or call this load?
func game_setup():
	terrain = $Field/Terrains/Terrain1
	terrain.show()
	for pos in terrain.get_used_cells():
		init_plant(pos)

	update_plants()
	setup_upgrades()

	game_start()


func game_start() -> void:


	score = 0
	score_ui_update()

	add_child(playtime_stopwatch)
	playtime_stopwatch.start()

	if not Score.best_score:
		help_text.show()
		var tween = help_text.create_tween().set_loops().set_trans(Tween.TRANS_SINE)
		var start_pos = help_text.rect_position
		tween.tween_property(help_text, "rect_position", start_pos + Vector2(0, 10), 1)
		tween.tween_property(help_text, "rect_position", start_pos, 1)

#	G.music_player.play_and_switch("Song", "Main")
#	goto_section(section_name: String, when: int = NDef.When.ODD_BAR)
	G.music_player.goto_section("Main", NDef.When.LOOP)

func game_over():
	playtime_stopwatch.stop()

	stats.playtime = playtime_stopwatch.time
	stats.upgrades_unlocked = 0
	for id in upgrades_enabled:
		stats.upgrades_unlocked += upgrades_enabled[id]

	var final_score = stats.plants_harvested * stats.upgrades_unlocked

	Score.new_score(final_score, stats)

	var total_upgrades = 0
	for id in upgrades:
		total_upgrades += upgrades[id].levels

	F.change_scene(Config.scenes.ending, { 'total_upgrades': total_upgrades, 'stats': stats })


func pay(amount):
	add_score(-amount)


func add_score(change):
	score += change

	if change > 0:
		stats.total_score += change

	if score >= upgrades["victory"].cost:
		for u in upgrades_box.get_children():
			if u.id == "victory":
				u.text = upgrades["victory"].name

	score_ui_update()
	var s = $"%Score"
	var label = s.get_node("Value")
	label.rect_pivot_offset = label.rect_size / 2.0
	var t = create_tween().set_trans(Tween.TRANS_SINE)
	t.tween_property(label, "rect_scale", Vector2(1.8, 1.8), 0.1)
	t.tween_property(label, "rect_scale", Vector2.ONE , 0.1)


func score_ui_update():
	var s = $"%Score"
	s.get_node("Value").text = str(score)

	refresh_upgrades()


func refresh_upgrades():
	for child in upgrades_box.get_children():
		var id = child.id
		var cost = child.current_cost
		if score >= cost or cheats:
			child.disabled = false
		else:
			child.disabled = true

