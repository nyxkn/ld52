extends Object
class_name D

# this is a class_name and not an autoload, so we don't track state in here
# this only holds definitions
# e.g. const, enums, class data types

# for stateful values use global instead

const E = 2.718281
