extends Node

# things to do at startup


# setup config values in here. do we still need this now that we're saving config in res?
func config_setup():
	pass

	# always disable the debug settings in release build
	if not Config.debug_build:
		Config.straight_to_game = false
		Config.test_bench = false

	Log.i("config values set", name)
	Config.save_cfg()


# load things in here
func load_things():
	Log.d("loading things", name)

	SoundManager.load_samples_directory("res://assets/sfx/")
	SoundManager.load_samples_directory("res://assets/jfxr/")

	# load particles and save them. this is to make sure all of the latest changes have been saved
	# take note that the save only runs in debug builds
	var ParticlesWorkspace = preload("res://tools/particles_workspace/particles_workspace.tscn")
	var pw = ParticlesWorkspace.instance()
	pw.visible = false
	# to get _ready to run and save properly we have to add_child. then we remove immediately
	add_child(pw)
	pw.queue_free()

	# load test images. 10 files for about 200mb total
#	for i in range(1, 10):
#		# $TextureRect.texture = load("res://assets/temp/chan/" + str(i) + ".png")
#		load("res://assets/temp/chan/" + str(i) + ".png")
#		print("loaded asset " + str(i))


# things to do after loading is finished
func _loading_finished():
	G.totaltime_stopwatch.start()

	# hack because otherwise html build on itch has full volume
	SettingsAudio.set_volume("Master", 0.4)
	SettingsAudio.set_volume("SFX", 0.4)
