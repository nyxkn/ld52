extends Node

# this is meant to be an autoload
# define global variables in here
# this is also useful for static variables for classes (can't have them inside the actual classes)

# for stateless definitions, use definitions.gd instead


var totaltime_stopwatch := StopwatchRT.new()
onready var music_player: Node = $MusicPlayer


func _ready() -> void:
	pass
	yield(F.wait(1.0), "completed")
	music_player.play_and_switch("Song", "Intro")
