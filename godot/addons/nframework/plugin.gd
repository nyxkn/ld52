tool
extends EditorPlugin

const BASE_PATH: String = "res://addons/nframework/"
const AUTOLOADS_PATH: String = BASE_PATH + "autoloads/"
const NULL_AUTOLOAD: String = "null.gd"

# order matters!
# the autoloads will be loaded and appear in the scenetree in this order
# in any case, the scenetree, get_tree(), won't be accessible until all autoloads have finished loading
# you can disable autoloads by adding them to disabled_autoloads
# almost all autoloads require the core modules, so don't disable those
# you mostly just want to disable the overlays
const autoloads := {
	## core modules
	# logger should be the first thing loaded, because everything should be able to print
	# it's okay if log can't read config values. just keep that into account
	"Log": "logger.gd",
	"Config": "config.gd",

	# runtime settings
	"SettingsAudio": "settings/settings_audio.gd",
	"SettingsControls": "settings/settings_controls.gd",
	"SettingsVideo": "settings/settings_video.gd",
	"Settings": "settings/settings.gd",

	# overlays
	"TransitionOverlay": "overlay/transition_overlay.tscn",
	"FPSOverlay": "overlay/fps_overlay.tscn",
	"DebugDraw": "overlay/debug_draw/debug_draw.tscn",
	"PauseScreen": "overlay/pause_screen.tscn",
	"TestBench": "overlay/test_bench.tscn",

	# systems
	# framework depends on transition overlay and pause system
	"F": "framework.gd",
	"SoundManager": "sound_system/sound_manager.gd",
#	"Music": "sound_system/music.gd",
	"Score": "score/score.gd",
}

const disabled_autoloads := [
#	"Settings",
#	"SettingsAudio",
#	"SettingsControls",
#	"SettingsVideo",
#	"Pause",
#	"Transition",
#	"FPS"
#	"F",
	]


var dock_file_editor
var dock_config
var dock_resolution


func _ready():
	print('plugin ready')
	# we could set it from here, but then it's lost when config.gd gets reloaded
	# so we just set a new one from config instead
#	Config.editor_interface = get_editor_interface()


func _enter_tree():
	# Initialization of the plugin goes here.

	for key in autoloads.keys():
		if key in disabled_autoloads:
			add_autoload_singleton(key, AUTOLOADS_PATH + NULL_AUTOLOAD)
		else:
			add_autoload_singleton(key, AUTOLOADS_PATH + autoloads[key])
	add_autoload_singleton("G", "res://global.tscn")


	# Load the dock. Load from file here so it's reloaded on every _enter_tree (?)
	dock_resolution = load("res://addons/nframework/dock/resolution.tscn").instance()
	add_control_to_dock(DOCK_SLOT_LEFT_BR, dock_resolution)

	dock_file_editor = load("res://addons/nframework/dock/file_editor.tscn").instance()
	add_control_to_dock(DOCK_SLOT_LEFT_BR, dock_file_editor)

	dock_config = load("res://addons/nframework/dock/config.tscn").instance()
	add_control_to_bottom_panel(dock_config, "Config")


func _exit_tree():
	# Clean-up of the plugin goes here.

	remove_autoload_singleton("G")
	for key in autoloads.keys():
		remove_autoload_singleton(key)

	# Remove the dock.
	remove_control_from_bottom_panel(dock_config)
	dock_config.queue_free()

	remove_control_from_docks(dock_resolution)
	dock_resolution.queue_free()
	remove_control_from_docks(dock_file_editor)
	dock_file_editor.queue_free()
