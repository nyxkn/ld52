# audio

- button_002.wav - sonniss gdc 2019, 3maze, app fx

# fonts

- managore m3x6, m5x7, m6x11 - https://managore.itch.io/ 
- pixellocale - https://fontstruct.com/fontstructions/show/1765530/pixellocale-v-1-4
- monogram - https://datagoblin.itch.io/monogram
- silver - https://poppyworks.itch.io/silver
- 04b_03 - https://www.dafont.com/04b-03.font
- gravity - https://jotson.itch.io/gravity-pixel-font
- saint11 - https://saint11.org/blog/fonts/
- kenney - https://kenney.nl/assets/kenney-fonts
- pico-8 - https://github.com/MrSimbax/pico-8-font
- dogica - https://www.dafont.com/dogica.font
- cg pixel - https://graphicdesign.stackexchange.com/questions/91478/a-font-thats-readable-with-a-5px-height
- abaddon - https://caffinate.itch.io/abaddon

# icons
- cog_font_awesome - https://commons.wikimedia.org/wiki/File:Cog_font_awesome.svg
