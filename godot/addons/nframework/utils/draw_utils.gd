class_name DrawUtils


static func draw_triangle(canvas: CanvasItem, points: PoolVector2Array, color: Color = Color.white):
	var v1 = points[0]
	var v2 = points[1]
	var v3 = points[2]

	canvas.draw_line(v1, v2, Color.white, 1)
	canvas.draw_line(v2, v3, Color.white, 1)
	canvas.draw_line(v3, v1, Color.white, 1)


static func draw_points(canvas: CanvasItem, points: PoolVector2Array, color: Color = Color.white, size: int = 2):
	size = max(size, 2)
	for p in points:
#		canvas.draw_circle(v, size, color)
		var rect_size = Vector2(size, size)
		canvas.draw_rect(Rect2(p - rect_size/2.0, rect_size), color)
