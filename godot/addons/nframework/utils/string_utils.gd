class_name StringUtils


## string or regex
## if regex we just return it
## if string we compile the regex
static func compile_regex(regex_string: String) -> RegEx:
	var re := RegEx.new()
	re.compile(regex_string)
	if not re.is_valid():
		Log.e(['warning: regex', regex_string, 'is not valid'], 'compile_regex')
	return re


static func get_filename(file_path: String, with_ext = true) -> String:
	var split = file_path.split("/")
	var file_name = split[-1]
	if not with_ext:
		file_name = file_name.split(".")[0]
	return file_name


## case conversion functions lifted from:
## https://gist.github.com/me2beats/443b40ba79d5b589a96a16c565952419
static func snake_to_camel(string: String) -> String:
	var result = PoolStringArray()
	var prev_is_underscore = false
	for ch in string:
		if ch == '_':
			prev_is_underscore = true
		else:
			if prev_is_underscore:
				result.append(ch.to_upper())
			else:
				result.append(ch)
			prev_is_underscore = false
	return result.join('')


static func snake_to_pascal(string: String) -> String:
	var result = snake_to_camel(string)
	result[0] = result[0].to_upper()
	return result


static func is_alphanumeric(s: String) -> bool:
#	if ch.length() > 1:
#		Log.e("this function only accepts single characters")
#		return flase
#
#	var ascii = ch.to_ascii()[0]
#	if ascii >= 48 and ascii <= 57:
#		return true
#	elif ascii >= 65 and ascii <= 90:
#		return true
#	elif ascii >= 97 and ascii <= 122:
#		return true
#	return false


	var re = compile_regex("^[a-zA-Z0-9]*$")
#	var result =
	if re.search(s):
		return true
	return false


static func camel_to_snake(string: String) -> String:
	var result := PoolStringArray()
#	var result := ""
	var prev_is_number = false
#	var prev_is_symbol = false

	for i in string.length():
		var ch = string[i]
		var ascii = ch.to_ascii()[0]

		if ascii >= 48 and ascii <= 57:
			# this is a number
			if prev_is_number:
				# if this is a second number after a first number, leave unchanged
				result.append(ch)
			else:
				# mark this as being a number. 1 becomes _1
				# treating this the same as an uppercase letter
				# at times no underscore before a number looks better
				# but this is an automated translation
				# so let's be consistent and use the simple generic approach
				result.append('_' + ch)
				prev_is_number = true
		else:
			# this is a letter
			if is_alphanumeric(ch) and ch == ch.to_upper():
				# uppercase letter L becomes _l
				result.append('_' + ch.to_lower())
			else:
				# this is a lowercase letter or a symbol. leave as is
				result.append(ch)
			prev_is_number = false

	var result_str = result.join('')
	# remove accidental double underscores. e.g. if there's already an underscore before a number
	result_str = result_str.replace("__", "_")

	return result_str


static func pascal_to_snake(string: String) -> String:
	var s := camel_to_snake(string)
	s = s.lstrip("_")
	return s
