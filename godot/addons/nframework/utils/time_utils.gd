class_name TimeUtils

## this is used in format_seconds
enum SecondsResolution { S, MS, US }

## format a number of seconds into hh:mm:ss
## optionally also adding the fractional part in ms or us
static func format_seconds(seconds: float, resolution: int = SecondsResolution.US):
	# extract the fractional part
	var frac = seconds - int(seconds)
	if resolution == SecondsResolution.MS:
		frac = frac * 1000
		frac = "%03d" % frac
	elif resolution == SecondsResolution.US:
		frac = frac * 1000000
		frac = "%06d" % frac

	var seconds_whole = int(floor(seconds))

	var s = seconds_whole % 60
	var m = (seconds_whole / 60) % 60
	var h = seconds_whole / (60 * 60) # add %24 if you want to cap at hours and add days

	var colon := ":"
	var pad := "%02d"

	var formatted = str("+", h, colon, pad % m, colon, pad % s)
	if resolution != SecondsResolution.S: formatted += str(".", frac)

	return formatted


## format a number of float seconds into minutes plus float seconds (mm:ss.fractional)
## TODO this function and the above can probably be merged
static func seconds_to_minutes(seconds: float) -> String:
	var s = fmod(seconds, 60)
	var m = fmod(seconds, 60 * 60) / 60

	# this also adds the hours
	# var h = seconds / float(60 * 60)
	# var formatted = "%02d:%02d:%02d" % [h, m, s]

	# TODO in godot4 float padding works properly so we don't need this hack
	# this should end up working: var formatted = "%02d:%02.3f" % [m, s]
	var padded_s = str("%.3f" % s).pad_zeros(2)
	var formatted = "%02d:%s" % [m, padded_s]

	return formatted
