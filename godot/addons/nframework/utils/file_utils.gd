class_name FileUtils


# check if file exists
static func file_exists(file_path: String) -> bool:
#	var file = File.new()
#	return file.file_exists(file_path)
	var dir = Directory.new()
	return dir.file_exists(file_path)


static func dir_exists(dir_path: String) -> bool:
	var dir = Directory.new()
	return dir.dir_exists(dir_path)


## read content from file
static func read_file(file_path: String, password: String = ""):
	Log.i(["reading from file:", file_path], "read_file")
	var file = File.new()

	var err
	if password:
		err = file.open_encrypted_with_pass(file_path, File.READ, password)
	else:
		err = file.open(file_path, File.READ)

	if err != OK:
		print_file_error(err, file_path, "read_file")
		return

	var content = file.get_as_text()
	file.close()

	return content


## write content to file
static func write_to_file(content: String, file_path: String, password: String = ""):
	Log.i(["writing to file:", file_path], "write_to_file")
	var file = File.new()

	# opening with WRITE also truncates the file
	var err
	if password:
		err = file.open_encrypted_with_pass(file_path, File.WRITE, password)
	else:
		err = file.open(file_path, File.WRITE)

	if err != OK:
		print_file_error(err, file_path, "write_to_file")
		return

	file.store_string(content)
	file.close()


# add popup for confirmation? probably can't from a static function
static func delete_file(file_path: String):
	Log.w(["deleting file:", file_path])
	var dir = Directory.new()
	dir.remove(file_path)


static func get_files_in_dir_recursive(dir_path: String, ext: String = "", regex = null) -> Array:
	var files := []

	# running once without constraints so that we also read directories
	var dir_files = get_files_in_dir(dir_path)
	for f in dir_files:
		if dir_exists(f):
			files.append_array(get_files_in_dir_recursive(f, ext, regex))

	# running a second time with the required constraints
	files.append_array(get_files_in_dir(dir_path, ext, regex))

	return files
#	return get_files_in_dir_recursive(


## find files in directory. return matching filenames
## if no ext or regex is provided, list all files (except .import)
## if ext provided we match file extension. if ext passed has no leading dot we'll add it.
## if regex provided we match regex (ignores ext param)
## TODO named parameters would be better here
## regex is either a compiled RegEx or a string
static func get_files_in_dir(dir_path: String, ext: String = "", regex = null) -> Array:
	if regex is String:
		regex = Utils.compile_regex(regex)
	if ext and not ext.begins_with("."):
		ext = "." + ext

#	dir_path = dir_path.trim_suffix("/")

	var dir: Directory = Directory.new()

	var files := []

	var err = dir.open(dir_path)
	if err == OK:
		dir.list_dir_begin(true)
		var file_name = dir.get_next()
		while file_name != "":
			var matching = false
			if !ext and !regex and !file_name.ends_with(".import"):
				matching = true
			elif regex and regex.search(file_name):
				matching = true
			elif ext and file_name.ends_with(ext):
				matching = true

			if matching:
				files.append(dir_path + "/" + file_name)

			file_name = dir.get_next()
	else:
		print_file_error(err, dir_path, "get_files_in_dir")
#		Log.e(["error", err, "opening dir_path"], "get_files_in_dir")

	return files


## https://docs.godotengine.org/en/stable/classes/class_packedscene.html
## this saves the node and all the nodes it owns
## if you want the children to be saved, make sure node owns them
static func save_scene(dir_path: String, node: Node, tscn: bool = false) -> void:
	if not dir_exists(dir_path):
		Log.e(["directory", dir_path, "does not exist"], "save_scene")
		return

	var scene = PackedScene.new()
	var scene_name = StringUtils.pascal_to_snake(node.name)
	var extension = ".scn"
	if tscn:
		extension = ".tscn"
	var file_path = dir_path + "/" + scene_name + extension

	var result = scene.pack(node)
	if result == OK:
		var err = ResourceSaver.save(file_path, scene)
		if err != OK:
			Log.e(["an error occurred while saving scene", file_path, "to disk"], "save_scene", err)


# data is an array of dictionaries, saved as one per line
# this results in a file that's potentially hard to human-read
# while to_json can technically convert any variable to json,
# you probably want to stick with dictionaries
# this automatically gives you the standard key-value json format
static func save_json_multiline(file_path: String, data: Array) -> void:
	if ! file_path.ends_with(".json") and ! file_path.begins_with(Config.data_path):
		Log.e("for safety, save the json to data/ and use a .json extension", "save_json")
		return

	for dict in data:
		if ! dict is Dictionary:
			Log.e("save_json will only save dictionaries. aborting", "save_json")
			return

	var file = File.new()

	var err = file.open(file_path, file.WRITE)
	if err != OK:
		print_file_error(err, file_path, "save_json_multiline")
		return

	for d in data:
		file.store_line(to_json(d))
	file.close()

	Log.d(["json successfully saved to", file_path], "save_json")


# returns an array of dictionaries
# json file should contain one dictionary per line
static func read_json_multiline(file_path: String) -> Array:
	var file = File.new()

	var err = file.open(file_path, file.READ)
	if err != OK:
		print_file_error(err, file_path, "read_json_multiline")
		return []

	var data = []
	while file.get_position() < file.get_len():
		# Get the saved dictionary from the next line in the save file
		var json_data = parse_json(file.get_line())
		data.append(json_data)
	file.close()

	return data


# writes pretty printed json to file
# this takes a single dictionary as data
static func save_json(file_path: String, data: Dictionary) -> void:
	if ! file_path.ends_with(".json") and ! file_path.begins_with(Config.data_path):
		Log.e("for safety, save the json to data/ and use a .json extension", "save_json")
		return

	var file = File.new()

	var err = file.open(file_path, file.WRITE)
	if err != OK:
		print_file_error(err, file_path, "save_json")
		return

	file.store_string(JSON.print(data, "\t"))
	file.close()

	Log.d(["json successfully saved to", file_path], "save_json")


# reads pretty printed json from file
# (or non-pretty printed, but only one dictionary)
static func read_json(file_path: String) -> Dictionary:
	var file = File.new()

	var err = file.open(file_path, file.READ)
	if err != OK:
		print_file_error(err, file_path, "read_json")
		return {}

	# does parse_json always return a dictionary?
	var json_data: Dictionary = parse_json(file.get_as_text())
	file.close()

	return json_data


# data has to be a dictionary of dictionaries
# if you need to save different data types from that, you need to manually implement this
# it's quite simple to do though
static func save_cfg(file_path: String, data: Dictionary, password: String = "") -> void:
	var config = ConfigFile.new()

	for section in data:
		for key in data[section]:
			var value = data[section][key]
			config.set_value(section, key, value)

	if password:
		config.save_encrypted_pass(file_path, password)
	else:
		var err = config.save(file_path)
		if err != OK:
			print_file_error(err, file_path, "save_cfg")



# this reads a configfile into a dictionary of dictionaries
static func read_cfg(file_path: String, password: String = "") -> Dictionary:
	var config = ConfigFile.new()

	var err
	if password:
		err = config.load_encrypted_pass(file_path, password)
	else:
		err = config.load(file_path)
	if err != OK:
		print_file_error(err, file_path, "read_cfg")
		return {}

	var data = {}
	for section in config.get_sections():
		data[section] = {}
		var keys = config.get_section_keys(section)
		for key in keys:
			data[section][key] = config.get_value(section, key)

	return data


# simply create a script that extends resource
# with no logic and mark variables you want exported with export(type)
# non-export variables won't be saved
# resources are references, and references are unique
# so you cannot instance different versions. you always get a reference to the same thing
static func save_resource(file_path: String, resource: Resource) -> void:
	ResourceSaver.save(file_path, resource)


static func load_resource(file_path: String):
	if ! file_path.ends_with(".tres"):
		# also allow .res?
		Log.e(["make sure you're trying to load a .tres file. current attempt:", file_path], "save_json")
		return

	var resource = load(file_path)
	return resource


# takes an array of variables (any types)
# a note in the docs says that only variables marked with PROPERTY_USAGE_STORAGE will be saved
# not sure what this means
# https://docs.godotengine.org/en/stable/classes/class_file.html#class-file-method-store-var
# objects won't be saved by default but can be. see store_var below
static func save_binary(file_path: String, data: Array) -> void:
	var file = File.new()

	# opening with WRITE also truncates the file
	var err = file.open(file_path, file.WRITE)
	if err != OK:
		print_file_error(err, file_path, "save_binary")
		return

	for d in data:
		# to store full objects, call with second argument true
		# file.store_var(d, true)
		# this can potentially include code
		file.store_var(d)
	file.close()

	Log.d(["binary file successfully saved to", file_path], "save_binary")


# objects won't be loaded by default but can be. see get_var below
static func load_binary(file_path: String) -> Array:
	var file = File.new()

	var err = file.open(file_path, file.READ)
	if err != OK:
		print_file_error(err, file_path, "load_binary")
		return []

	var data = []
	while file.get_position() < file.get_len():
		# to read full objects, call get_var with argument true
		# file.get_var(true)
		# this will run code if it was included in the object
		# so never use this on untrusted sources
		data.append(file.get_var())
	file.close()

	return data


static func print_file_error(err: int, file_path: String = "", category = "FileUtils"):
	if category != "FileUtils":
		category = "FileUtils." + category

	Log.e(["error while opening file: ", file_path], category, err)
