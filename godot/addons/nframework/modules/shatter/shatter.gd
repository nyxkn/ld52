extends Node2D


## https://www.reddit.com/r/godot/comments/nimkqg/how_to_break_a_2d_sprite_in_a_cool_and_easy_way/


const Shard = preload("res://addons/nframework/modules/shatter/shard.tscn")

# sets the number of break points. resulting shards will be (subdivisions + 1) * 2. it's always an even number
export(int, 200) var subdivisions := 3
#export(Vector2) var threshold := Vector2(0.1, 0.1)
export(float) var min_impulse := 10.0 # impulse of the shards upon breaking
export(float) var max_impulse := 200.0
export(float) var min_torque_magnitude := 100.0
export(float) var max_torque_magnitude := 200.0
#export(float) var lifetime := 5.0 # lifetime of the shards
export(bool) var debug_draw := true # debugging: display sprite triangulation

var triangles = []
var shards = []
var points = []

onready var sprite: Sprite = get_parent()
#onready var sprite: Sprite = $"../Sprite"


func _ready() -> void:
	assert(sprite and sprite is Sprite, "shatter node needs to be a child of a sprite")
#	call_deferred("_generate_shards")


func _draw() -> void:
	if debug_draw:
		DrawUtils.draw_points(self, points, Color.white, 4)

		for t in triangles:
			DrawUtils.draw_triangle(self, t)


# for testing
#func _unhandled_input(event: InputEvent) -> void:
#	if event is InputEventKey and event.is_pressed():
#		if event.scancode == KEY_B:
#			shatter()


func shatter() -> void:
	# you can also move generate_shards to ready so you precompute everything in advance
	_generate_shards()

	sprite.self_modulate.a = 0
#	modulate = Color.red
	for shard in shards:
		shard.get_node("CollisionPolygon2D").disabled = false
		shard.get_node("VisibilityNotifier2D").connect("screen_exited", self, "on_Shard_screen_exited", [shard])
		shard.mode = RigidBody2D.MODE_RIGID

		# wait a frame after activating physics before sending impulses
		yield(get_tree(), "physics_frame")

		# disable the collision mask if you don't want shards to collide with each other
		# but you probably want to actually change shard's collision layer so it doesn't interfere with your game
#		shard.collision_mask = 0b0
		var direction = Vector2.UP.rotated(F.rng.randf_range(0, TAU))
		var impulse = rand_range(min_impulse, max_impulse)
		shard.apply_central_impulse(direction * impulse)
		# apply torque requires an enabled collision shape,
		shard.apply_torque_impulse(
			((F.rng.randi_range(0, 1) * 2) - 1) * F.rng.randf_range(min_torque_magnitude, max_torque_magnitude))
		shard.show()


func _generate_shards() -> void:
	var rect = sprite.get_rect()

	# add outer frame points
	points.append(rect.position)
	points.append(rect.position + Vector2(rect.size.x, 0))
	points.append(rect.position + Vector2(0, rect.size.y))
	points.append(rect.end)

	# this is how far away from the edge we start splitting, in % of the size of the sprite
	# prevents slim triangles being created at the sprite edges
	# shard_size is the average size assuming an even subdivision.
	# divided by two because e.g. if there's 6 triangles, that only means 3 per axis
	# the rationale here is that if there's lots of subdivisions and you keep a big percentage
	# you'll have much bigger triangles at the edges
	var shard_size = 1.0 / ((subdivisions+1) * 2 / 2)
	var threshold = Vector2(min(0.1, shard_size), min(0.1, shard_size))
	var offset = rect.size * threshold

	# add random break points, taking edge distance threshold into account
	for i in subdivisions:
		var p = rect.position + Vector2(
			F.rng.randi_range(0 + offset.x, rect.size.x - offset.x),
			F.rng.randi_range(0 + offset.y, rect.size.y - offset.y))
		points.append(p)

	# calculate triangles
	var delaunay = Geometry.triangulate_delaunay_2d(points)
	for i in range(0, delaunay.size(), 3):
		triangles.append([points[delaunay[i]], points[delaunay[i + 1]], points[delaunay[i + 2]]])

	# create RigidBody2D shards
	for t in triangles:
		var shard = Shard.instance()
		shard.hide()
		add_child(shard)
		shards.append(shard)

		# setup polygons & collision shapes
		var polygon_2d = shard.get_node("Polygon2D")
		var collision_polygon_2d = shard.get_node("CollisionPolygon2D")
		var visibility_notifier_2d = shard.get_node("VisibilityNotifier2D")

		polygon_2d.texture = sprite.texture
#		polygon_2d.color = Utils.random_color()
		polygon_2d.polygon = t
		# uv coordinates have top left corner at (0,0) of the texture and bottom right corner at (size,size)
		# also note that in shaders, uv coordinates are normalized [0,1], so the same two corners are (0,0) and (1,1)
		# this is different from texture rect coordinates where (0,0) is at the center
		# so we need to offset our coordinates to get the correct uv values
		var uv_t := []
		for v in t:
			uv_t.append(v + rect.size / 2.0)
		polygon_2d.uv = uv_t

		# you can shrink the polygons by a pixel or two so that they don't overlap. does it matter though?
#		collision_polygon_2d.polygon = Geometry.offset_polygon_2d(t, -2)
		collision_polygon_2d.polygon = t

		# setting this to the whole texture just cause it's easier, even if it's actually smaller
		visibility_notifier_2d.rect = rect

	update()


# alternatively, you could have a timer that simply cleans up all shards at once like after 10s from shatter
# or you could want to not delete them at all and keep them around!
func on_Shard_screen_exited(shard) -> void:
	shard.queue_free()
