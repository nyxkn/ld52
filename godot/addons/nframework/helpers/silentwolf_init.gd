extends Node


var setup_finished := false
var thread: Thread


func _ready() -> void:
	name = "SilentWolfInit"

	if Config.threaded_loading:
		thread = Thread.new()
		thread.start(self, "thread_function")
	else:
		silentwolf_setup()


func silentwolf_setup():
	# grabbing the silentwolf singleton through the scenetree so that
	# godot editor doesn't complain if plugin isn't enabled (singleton loaded)
	var SilentWolf = get_tree().root.get_node("SilentWolf")

	# =!!!= you need to rename _ready to init in the SilentWolf autoload
	# this is so we can manually call it when we want rather than letting it run automatically
	# you have to do this again if you update SilentWolf

	# for some reason assert won't print the error message
	# but still stop the code (of the thread only)
#		assert(SilentWolf.has_method("init"), "error initializing silentwolf: ensure you have renamed _ready to init in SilentWolf.gd")

	# makeshift assert
	if ! SilentWolf.has_method("init"):
		Log.e("error initializing silentwolf: ensure you have renamed _ready to init in SilentWolf.gd")
		return # warning: this doesn't queue_free the object at the end

	SilentWolf.init()

	var silentwolf_json = FileUtils.read_json("res://data/silentwolf.json")
	SilentWolf.configure(silentwolf_json)

	SilentWolf.configure_scores({
		"open_scene_on_close": Config.scenes.main_menu
	})

#	SilentWolf.configure_auth({
#		"redirect_to_scene": "res://scenes/MainPage.tscn",
#		"email_confirmation_scene": "res://addons/silent_wolf/Auth/ConfirmEmail.tscn",
#		"login_scene": "res://addons/silent_wolf/Auth/Login.tscn",
#		"reset_password_scene": "res://addons/silent_wolf/Auth/ResetPassword.tscn",
#		"session_duration_seconds": 0,
#		"saved_session_expiration_days": 30
#	})

	setup_finished = true


# we're running this as a thread because it locks for a few seconds when network is down
func thread_function(userdata):
	silentwolf_setup()

	call_deferred("thread_end")
	return true


func thread_end():
	var ret = thread.wait_to_finish()
	queue_free()
