extends State


# careful with using _ready or onready because it gets executed early
# before the rest of the scene is ready
# setup is guaranteed to happen after the whole scene has loaded


func setup() -> void:
	.setup()


func enter(msg = {}) -> void:
	.enter(msg)


func exit() -> void:
	.exit()


func input(event: InputEvent) -> void:
	pass


func process(delta: float) -> void:
	pass


func physics_process(delta: float) -> void:
	pass
