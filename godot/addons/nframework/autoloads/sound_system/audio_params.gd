extends Resource
class_name AudioParams

# change of volume in db
var gain: float = 0.0

# pitch offset in semitones. can be fractions of semitones too, which correspond to cents
# e.g. 1.52 semitones is 152 cents
var pitch_offset: float = 0.0

# maximum deviation of pitch in semitones, applied equally up and down
# can be fractions of semitones too
# this technically affects sample rate, so it also changes time
# 12 would make the higher octave sound half as long, while the lower octave becomes twice as long
# keep this in mind, so maybe avoid large values to avoid huge length differences
var pitch_randomization: float = 0.0

# maximum deviation of volume in db. this we apply only downward
# because if we assume samples to be normalized to 0db, upping the gain will create distortion
# so 10 would make us randomize between 0db and -10db
# db notes:
# +3db means double the power (energy). but +10db is what's perceived as twice as loud
# a 10db change is a factor of 2 change in perceived loudness (+10db is double as loud)
# a 6db change is a factor of 2 change in voltage (+6db is double the voltage)
# a 3db change is a factor of 2 change in power ratio (+3db is double the power)
var volume_randomization: float = 0.0
