extends Node

## set this to a low value like 2 to simulate retro consoles
## new sound effect will replace the old ones
## otherwise 32 seems like a sane number
var max_players: int


# var players := []
var pool_size: int = 0
var bus: String = ""

var available_players := []
var busy_players := []


func _init(bus: String, max_players: int = 3, pool_size: int = 3) -> void:
	self.bus = bus
	var players_num = min(pool_size, max_players)
	for i in players_num:
		add_player()


func add_player() -> void:
	var player := AudioStreamPlayer.new()
	add_child(player)

	available_players.push_back(player)
	pool_size += 1

	player.bus = bus
# 	player.volume_db = linear2db(1)
#	player.pitch_scale = 1 + (pool_size - 1) * 0.2
	player.connect("finished", self, "_playing_finished", [player])

#	Log.d(["pool size", pool_size])


func _playing_finished(player: AudioStreamPlayer) -> void:
#	Log.d([ "player finished", player ], name)
	busy_players.erase(player)
	available_players.push_back(player)


func get_available_player() -> AudioStreamPlayer:
	var p: AudioStreamPlayer

	if available_players.size() == 0:
		# if < max players, add player
		if pool_size < max_players:
			Log.d("all players busy, adding new")
			add_player()
			p = available_players.pop_front()
		else:
			Log.d("all players busy, recycling")
			p = busy_players.pop_front()
			# calling stop is redundant. the new play() is going to stop the previous sound
			# p.stop()
	else:
		p = available_players.pop_front()

	busy_players.push_back(p)

	return p


func play(stream: AudioStream, ap: AudioParams = AudioParams.new()) -> AudioStreamPlayer:
	var p := get_available_player()

	p.stream = stream

	p.volume_db = 0.0 + ap.gain

	p.pitch_scale = pow(2, ap.pitch_offset / 12.0)

	if ap.pitch_randomization != 0.0:
		# converting our pitch_randomization value to a pitch multiplier by using 2^x
		var min_pitch = pow(2, (ap.pitch_offset - ap.pitch_randomization) / 12.0)
		var max_pitch = pow(2, (ap.pitch_offset + ap.pitch_randomization) / 12.0)
		p.pitch_scale = rand_range(min_pitch, max_pitch)


	if ap.volume_randomization != 0.0:
		p.volume_db = p.volume_db + rand_range(-ap.volume_randomization, 0.0)

	p.play()
	return p
