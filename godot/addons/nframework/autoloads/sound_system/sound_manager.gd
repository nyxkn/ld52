extends Node


const AudioPlayer := preload("audio_player.gd")

enum Mode { RETRO, PER_SAMPLE }
export(Mode) var mode := Mode.PER_SAMPLE

var samples := {}
var sample_players := {}

onready var sound_player: AudioPlayer = AudioPlayer.new("SFX", 3)
onready var ui_sound_player: AudioPlayer = AudioPlayer.new("UI", 3)


func _ready() -> void:
	add_child(sound_player)
	add_child(ui_sound_player)


# resource can be either the name of the loaded sample, or an audiostream
# see _play_sample below
func play(resource, audio_params: AudioParams = AudioParams.new()) -> void:
	if mode == Mode.RETRO:
		_play_sample(resource, sound_player, audio_params)
	elif mode == Mode.PER_SAMPLE:
		_play_sample(resource, sample_players[resource], audio_params)


func play_ui(resource) -> void:
	_play_sample(resource, ui_sound_player)


func _play_sample(resource, audio_player: AudioPlayer, audio_params: AudioParams = AudioParams.new()) -> void:
	if resource is AudioStream:
		audio_player.play(resource, audio_params)
	elif resource is String:
		if samples.has(resource):
			audio_player.play(samples[resource], audio_params)
		else:
			Log.e(["sample not loaded:", resource], name)
			# load on the spot? maybe bad idea


# add samples to the collection. we do the loading
func load_samples(resource_paths: Array) -> void:
	for r in resource_paths:
		r = r.trim_suffix(".import")
		# this gets the filename without extension
		var id = r.get_file().get_basename()

		if samples.has(id):
			Log.e(["sample with id", id, "already exists"], name)

		# TODO we might want to do this asynchronously in a thread
		# gametemplate has an example
		samples[id] = load(r)
		Log.d(["loaded sample:", id])

		if mode == Mode.PER_SAMPLE:
			sample_players[id] = AudioPlayer.new("SFX", 6)
			add_child(sample_players[id])


func load_samples_directory(folder_path: String) -> void:
	Log.d(["loading samples from directory", folder_path])
	var ext = ".wav.import"
	var files = FileUtils.get_files_in_dir(folder_path, ext)
	if files.empty():
		Log.w(["directory", folder_path, "contains no files with extension", ext])
	else:
		load_samples(files)


# add samples to the collection. you handle the loading
# pass in an array of Resource objects
func add_samples(resources: Array) -> void:
	for r in resources:
		r = r as Resource
		var id = r.get_path().get_file().get_basename()
		samples[id] = r


# free samples from memory
# pass in an array of samples ids
func remove_samples(ids: Array) -> void:
	for id in ids:
		samples[id].queue_free()
		samples.erase(id)
