extends Node

# pretending this is a class - so we know how it's structured
# but not using an actual class because saving that to file is a pain
const SCORE_DATA := {
	"timestamp": 0,
	"score": 0.0,
#	"metadata": {},
#	"online_score_id": "",
}


var best_score := {}
var last_score := {}

onready var save_path = Config.user_path + "scores.json"


func _ready() -> void:
	load_scores()


func save_scores() -> void:
	var all_scores := [last_score]

	if FileUtils.file_exists(save_path):
		var data = FileUtils.read_json(save_path)
		if data.all_scores:
	#		all_scores.push_front(data.all_scores)
			all_scores.append_array(data.all_scores)

	var scores = {"best_score": best_score, "last_score": last_score, "all_scores": all_scores}
	FileUtils.save_json(save_path, scores)


func load_scores() -> void:
	if not FileUtils.file_exists(save_path):
		return

	var data = FileUtils.read_json(save_path)
	if data.last_score and data.best_score:
		last_score = data.last_score
		best_score = data.best_score


func new_score(score: int, metadata := {}) -> void:
	last_score = score_data_new(OS.get_unix_time(), score, metadata)

	if not best_score or last_score.score > best_score.score:
		best_score = last_score

	if Config.silentwolf:
		var online_score_id = save_score_online("player_name", score, "", metadata)
#		if online_score_id:
#			last_score.online_score_id = online_score_id

	save_scores()


func score_data_new(timestamp, score, metadata) -> Dictionary:
	var score_data = SCORE_DATA.duplicate(true)
	score_data.timestamp = timestamp
	score_data.score = score
	if metadata:
		score_data.metadata = metadata
	return score_data


func save_score_online(player_name: String, score: int, board_name: String = "", metadata: Dictionary = {}) -> String:
#	local_scores.append({"player_name": player_name, "score": score, "metadata": metadata, "timestamp": OS.get_unix_time()})

	if not board_name:
		board_name = "main"

	var SilentWolf = get_tree().root.get_node("SilentWolf")
	var score_id = yield(SilentWolf.Scores.persist_score(player_name, score, board_name, metadata), "sw_score_posted")
	if score_id is GDScriptFunctionState:
		score_id = yield(score_id, "completed")

	score_id = str(score_id)
	# even after using str() this still gets saved as a gdscriptfunctionstate rather than a string
	# even after doing the second yield for completion
	# but it does print fine. what's going on?

	if score_id:
#		var score_id = str(score_id_return)
		Log.d("score saved online successfully: " + score_id, name)
		return score_id
	else:
		return ""

