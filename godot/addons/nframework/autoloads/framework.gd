extends Node

## Framework is pretty much just a collection of useful game functions or helpers

# export wouldn't work from here. it should be a scene
#export (String, FILE, "*.tscn") var main_menu: String = "res://addons/nframework/screens/MainMenu.tscn"
# or this should point to defaults, but a config file load could override this
# or poor man's version we just override this manually in game code

# you can possibly add changing scene through signals, that is:
#   emit_signal("change_scene", main_menu)
# this makes more sense if you're trying to decouple things,
# but since this is an autoload it's pointless really. just call directly


signal scene_faded_out
signal scene_changed
signal scene_faded_in
signal scene_transition_finished

signal game_paused(paused)

var paused: bool = false setget set_paused

## instance of rng everyone can use
## make sure you never call randomize on it from elsewhere
## we could enforce that by providing a wrapper to rng class
var rng: RandomNumberGenerator = RandomNumberGenerator.new()

## elapsed time in seconds since engine startup. microseconds resolution
## this will be affected by timescale changes and pause status
## if you need real time you want OS.get_ticks
## keep in mind that this won't get updated within the same function call
## e.g. if you try to time a difference: F.time, do_something, F.time
## F.time will be the same in both cases because we didn't get to run _process
var _time: float = 0.0 setget ,time

var layers := []

# this is the actual pixel size
var screen_size := Vector2(
	int(ProjectSettings.get_setting("display/window/size/width")),
	int(ProjectSettings.get_setting("display/window/size/height"))
	)

var changing_scene: bool = false

# this is the window size (can be different from pixel size)
# get_viewport() in here actually returns the root viewport
# still feels more semantically correct to use this
onready var viewport_size := get_viewport().get_visible_rect().size
onready var aspect_ratio: float = viewport_size.x / viewport_size.y


func _ready() -> void:
	# this is set to stop by default already
	# stop is necessary to make the time counter work properly
	# that is if you want it to stop during pause
	pause_mode = Node.PAUSE_MODE_STOP

	_setup_rng()

	# read the layer names into self.layers
	for i in range(1, 33):
		layers.append(ProjectSettings.get_setting("layer_names/2d_physics/layer_" + str(i)))


func _process(delta: float) -> void:
	_time += delta


func time() -> float:
	return _time


## returns ticks in seconds
func ticks() -> float:
	# get_ticks_msec is simply usec / 1000
#	return OS.get_ticks_usec() * 0.000001
	return OS.get_ticks_usec() * pow(10, -6)


func set_paused(value: bool) -> void:
	paused = value
	Log.d(["pause:", paused], name)
	# this also pauses all input
	get_tree().paused = paused
	# things can still receive inputs and act upon them when tree is paused
	# pause only stops all physics and stops calling all process and input functions
	# unless pause_mode is set to process like in this node
	emit_signal("game_paused", paused)


# shortcut for waiting
# use this like so:
# yield(F.wait(1.0), "completed")
func wait(time: float = 0.0) -> void:
	yield(get_tree().create_timer(time), "timeout")

# locking sleep. useful for testing
func sleep(time: float = 0.0) -> void:
	Log.d(["sleeping for", time], name)
	var start_time = ticks()
	while ticks() - start_time < time:
		pass
	Log.d(["finished sleeping"], name)



func change_scene_no_transition(scene, transfer_data: Dictionary = {}) -> void:
	change_scene(scene, transfer_data, 0)


# this is for changing the scene without forcing a free() call, and using queue_free() instead
# forcing free should generally work so do that, and it prevents both scenes from being in memory at the same time
# the use case for this though is if you're moving away from a scene that still has some threads running
# e.g. when moving away from main.gd when using threaded loading
func change_scene_soft(scene) -> void:
	change_scene(scene, {}, 0, 0, true)


# transition_duration: -1 for automatic, 0 for disabled, > 0 for duration
# scene can be a string path or a packedscene
func change_scene(scene, transfer_data: Dictionary = {},
			transition_duration: float = -1, black_duration: float = 0.5,
			soft_free: bool = false) -> void:

	if ! scene is String and ! scene is PackedScene:
		Log.e("scene is neither a string nor a packedscene", name)
		return

	if scene is String and ! ResourceLoader.exists(scene):
		Log.e(["attempting to load inexistent scene:", scene], name)
		return

	if changing_scene:
		Log.e(["trying to change to", scene, "but we are already in the middle of a scene change"], name)
		return

	Log.d(["changing scene to:", scene], name)
	changing_scene = true

	var with_transition = true
	if transition_duration == 0.0: with_transition = false
	var root_viewport: Viewport = get_tree().root

	root_viewport.gui_disable_input = true

	if with_transition:
		TransitionOverlay.fade_out(transition_duration)
		yield(TransitionOverlay, "fade_out_completed")
		emit_signal("scene_faded_out")

	# changing scene
	# warning: the actual changing of the scene is deferred, so you must wait for that to proceed
	# we need to use our custom scene switching because it provides the change_scene signal when change is done
	# so we know we can proceed. it's the only way to wait for the deferred run
#	get_tree().change_scene(scene_path)
	change_scene_custom(scene, transfer_data, soft_free)

#	if not get_tree().current_scene.filename == scene_path:
	yield(self, "scene_changed")
	var current_scene = get_tree().current_scene
	Log.d(["scene changed to:", current_scene], name)

	if with_transition and black_duration > 0:
		# change scene might take a few frames of black screen already
		# this is extra guaranteed black time
		yield(wait(black_duration), "completed")

	if with_transition:
		TransitionOverlay.fade_in(transition_duration)
		yield(TransitionOverlay, "fade_in_completed")
		emit_signal("scene_faded_in")

	root_viewport.gui_disable_input = false
	if paused: self.paused = false

	# this is when you can start running stuff in your new scene
	# (if you want to fade-in on things already running you can use scene_changed instead)
	Log.d(["scene transition finished:", scene], name)
	changing_scene = false
	emit_signal("scene_transition_finished")
	if current_scene.has_method("_scene_transition_finished"):
		current_scene._scene_transition_finished()


# scene can be either a path string or a packedscene
# if it's a string path we'll load it here
func change_scene_custom(scene, transfer_data: Dictionary = {}, soft_free: bool = false) -> void:
	# https://docs.godotengine.org/en/stable/tutorials/scripting/singletons_autoload.html#custom-scene-switcher
	# This function will usually be called from a signal callback,
	# or some other function in the current scene.
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.

	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:

	# we need to call deferred even if we're doing this in this Framework which is an autoload
	# that's because the old scene might still be finishing stuff
	# actually can we not simply do a wait for idle_frame in change scene instead of this messier deferred call?
	call_deferred("_change_scene_custom", scene, transfer_data, soft_free)


# this does exactly the same as what scenetree change_scene does, with an extra signal at the end
func _change_scene_custom(scene, transfer_data: Dictionary = {}, soft_free: bool = false) -> void:
	# It is now safe to remove the current scene
	var root = get_tree().root
	# current_scene is the last scene (unless you've manually added new scenes to root)
#	var current_scene: Node = root.get_child(root.get_child_count() - 1)
	var current_scene: Node = get_tree().current_scene

	# you generally want to call free() in here. we've already deferred this call so it should be safe
	# free() also cleanly unloads the old scene *before* we load the new one. so we never have both in memory
	# nevertheless, sometimes queue_free is necessary, if moving away from a scene that still has things running
	# e.g. threads that have been detatched (unless we find a way to fully detach them?)
#	Log.d(["attempting to free scene:", current_scene])
	if soft_free:
#		current_scene.call_deferred("free")
		current_scene.queue_free()
	else:
		current_scene.free()

	var packed_scene = scene
	if scene is String:
		packed_scene = ResourceLoader.load(scene)

	current_scene = packed_scene.instance()

	if not transfer_data.empty():
		if "scene_change_data" in current_scene:
			current_scene.scene_change_data = transfer_data
		else:
			Log.w(["failed to set scene_change_data on the new scene:", current_scene])

	root.add_child(current_scene)

	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	# scenetree current_scene is a node
	get_tree().current_scene = current_scene
	emit_signal("scene_changed")



# this returns the bitmask value of the layer
# layers are specified as a bitmask:
# layer 1 is bit 0 and has value 1 (2^0)
# layer 2 is bit 1 and has value 2 (2^1)
# layer 3 is bit 2 and has value 4 (2^2)
# etc.
# https://docs.godotengine.org/en/latest/tutorials/physics/physics_introduction.html#code-example
func get_layer(name) -> int:
	var idx = layers.find(name)

	# this code is to return the layer number. actually not very useful.
#	if idx == -1:
#		return idx
#	else:
#		# layers are numbered 1-32. no 0
#		return idx+1

	return int(pow(2, idx))


# randomizing the seed by default
# but you can also manually provide a seed number for reproducibility
func _setup_rng() -> void:
	# setting up global seed
	# we randomize first, then we get a random int that we then set as the seed
	# this way we know what the seed is and can reuse it later
	randomize()
	var random_seed = randi()
	seed(random_seed)
	Log.i(["Random seed:", random_seed])

	# setting up our own rng, optionally with a custom seed
	rng.randomize()
#	rng.seed = 1234


func p(array: Array) -> void:
	var msg = ""

	for e in array:
		msg += str(e) + " "
	msg.trim_suffix(" ")

	print(msg)

