tool
extends Node

# this is the place where we configure how nframework behaves
# these things should be saved and propagated to the builds
# so they cannot be saved in user://, but rather need to be saved in res:// and exported

# these are settings that are NOT available to the user and should never be changed on runtime
# so we should only save to disk from a debug build, never in release

# everything that you want saved/restored needs to be an export(type)


export(bool) var reload = false setget _reload
func _reload(_value) -> void:
	Log.d("config reloading")
	reload = false
	resetup_on_reload()


################################################################
# CONST

# random string
const CFG_SAVE_PASS := "eewoyienieyahnah"


################################################################
# LOGGING

# Global switch for showing logs
export(bool) var log_show = true

# All LogCategories are shown by default. Add true to this Dictionary to
# prevent showing  Logs of this Log Category
# removed for now
# var HIDE_LOG_CATEGORY = {}

# All LogLevels are shown by default. Add true to this Dictionary to
# prevent showing Logs of this Log Level
export(Dictionary) var log_hide_level := {}


################################################################
# RESOURCES and PATHS

export(Dictionary) var scenes := {}

var default_scenes := {
	intro = "res://addons/nframework/scenes/intro.tscn",
	ending = "res://addons/nframework/scenes/ending.tscn",
	loading = "res://addons/nframework/scenes/loading.tscn",
	main_menu = "res://addons/nframework/scenes/main_menu.tscn",
	settings_menu = "res://addons/nframework/scenes/settings_menu/settings_menu.tscn",

	# this should point to your game actual entry point
	game = "res://game/game.tscn"
	}


export(String) var levels_path := "res://levels/"
export(String) var data_path := "res://data/"
# this folder gets included in the build and in git
export(String) var config_path := "res://config/"
# this folder gets included in the build but NOT in git. maybe rename private?
export(String) var secret_path := "res://secret/"

# everything in user does NOT get included in the build
export(String) var user_path := "user://"
#export(String) var scores_path := "user://scores/"
export(String) var settings_cfg_save_path := "user://settings.cfg"

################################################################
# OTHER

export(bool) var low_res: bool = true


enum MenuControl {
	KEYBOARD = 1,
	MOUSE = 2,
	GAMEPAD = 4,
	}

# enable or disable menu keyboard controls
export(int, FLAGS, "keyboard", "mouse", "gamepad") var menu_control: int = (
	MenuControl.KEYBOARD | MenuControl.MOUSE
	)

#var PAUSE_ENABLED: bool = true

# Ludum Dare mode
# - disable loading of pre-made assets
# starting with framework/library type of base-code is allowed
# a "logo/intro screen" is allowed, but pre-made assets in menu probably isn't
# as long as the assets are so minimal to pass for a generic theme, we can probably ignore it
# sound effects are fine if they are made with a sound generator
#export(bool) var ludum_dare: bool = false

# enable silentwolf
export(bool) var silentwolf: bool = false

# jump straight into the game, skipping the menu or intro screens
export(bool) var straight_to_game: bool = false

# enable test_bench
export(bool) var test_bench: bool = false

export(bool) var simple_settings: bool = false

export(bool) var threaded_loading: bool = false


################################################################
# RUNTIME
# these are not settings we want to save
# they are read/initialized during runtime

# set if we're running as an html application
var html5: bool = false

var threads_enabled = false

# holds the selected theme
var theme: String

# holds the editor interface
var editor_interface

var debug_build: bool
var in_editor: bool

var audio_buses: Dictionary


################################################################
# LOGIC

var cfg_save_path = config_path + "config.ecfg"


func _ready() -> void:
	Log.d('config ready')
	resetup_on_reload()


# it's necessary to do this every reload (e.g. when you save the script)
# otherwise we lose the instance of editor interface
# but actually export is broken if we try to reference EditorPlugin
# some hints here: https://github.com/godotengine/godot-docs/issues/4082
# suggests that you should only use editorplugin from an editorplugin
# so we probably can't reload it here for convenience
func resetup_on_reload() -> void:
	pass
#	if in_editor:
#		var plugin: EditorPlugin = EditorPlugin.new()
#		editor_interface = plugin.get_editor_interface()
#		plugin.queue_free()


func _init() -> void:
	Log.d('config init')

	load_cfg()

	if OS.is_debug_build():
		debug_build = true

		# this should only be enabled in debug builds
		if Engine.editor_hint:
			in_editor = true

	if OS.get_name() == "HTML5":
		html5 = true

	if html5:
		threads_enabled = false
		threaded_loading = false
	else:
		threads_enabled = true

	if not scenes:
		scenes = default_scenes.duplicate()

	if low_res:
		theme = "res://addons/nframework/assets/themes/theme_lowres_tex.tres"
	else:
		# ideally theme_hires would just be an override on top of lowres, without redefining everything
		# should be possible in godot 4
		theme = "res://addons/nframework/assets/themes/theme_hires_tex.tres"


	# defaults: chars 2048, messages 10, errors 100, warnings 100
	# you're unlikley to have to change the last 3. but max chars per second is often limiting
	ProjectSettings.set_setting("network/limits/debugger_stdout/max_chars_per_second", 4096)
	ProjectSettings.set_setting("network/limits/debugger_stdout/max_messages_per_frame", 10)
	ProjectSettings.set_setting("network/limits/debugger_stdout/max_errors_per_second", 100)
	ProjectSettings.set_setting("network/limits/debugger_stdout/max_warnings_per_second", 100)

	ProjectSettings.set_setting("application/run/main_scene", "res://addons/nframework/main.tscn")

	# saving here so that we create the file the first time around
	# at this point it should contain just the defaults
	if debug_build:
		save_cfg()


# this needs to be saved to config/ or any other folder that explicitly gets added to the build
func save_cfg() -> void:
	var config = ConfigFile.new()

	for prop in Utils.get_export_variables(self):
		config.set_value("config", prop.name, get(prop.name))

#	config.save_encrypted_pass(cfg_save_path, OS.get_unique_id())
	config.save_encrypted_pass(cfg_save_path, CFG_SAVE_PASS)
#	config.save(cfg_save_path)

	# saving the file unencrypted when running the debug build for our convenience
	# this should not be shipped in export builds
	if debug_build:
		config.save(user_path + "config_unencrypted.cfg")

	# cannot use Log just yet, as config loads before logger
#	P.log_or_print("config saved")
	Log.d("config file saved")


func load_cfg() -> void:
	if not FileUtils.file_exists(cfg_save_path):
		return
#	var cfg = FileUtils.read_cfg(cfg_save_path, OS.get_unique_id())
	var cfg = FileUtils.read_cfg(cfg_save_path, CFG_SAVE_PASS)
#	var cfg = FileUtils.read_cfg(cfg_save_path)

	if cfg:
		var props = cfg["config"]
		for key in props:
			set(key, props[key])

	Log.d("config file loaded")
