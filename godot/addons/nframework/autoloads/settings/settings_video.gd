extends Node


export(bool) var fullscreen := OS.window_fullscreen setget set_fullscreen
export(bool) var borderless := OS.window_borderless setget set_borderless
export(bool) var vsync := OS.vsync_enabled setget set_vsync


func init() -> void:
	pass


func set_fullscreen(value: bool) -> void:
	fullscreen = value
	OS.window_fullscreen = fullscreen


func set_borderless(value: bool) -> void:
	borderless = value
	OS.window_borderless = borderless


func set_vsync(value: bool) -> void:
	vsync = value
	OS.vsync_enabled = vsync
