extends Node

signal actions_initialized

# necessary ui controls
# left down up right, accept, cancel
# in controllers, this is nowadays X for select, O for cancel
# but in older jrpg it's O for select and X for cancel === A and B in SNES, which makes sense
# (and possibly modern japanese ps systems too?)

#  X    Y     ^    3
# Y A  X B  [] O  2 1
#  B    A     X    0
# SNES XBOX PS    NUM

# https://docs.godotengine.org/en/stable/classes/class_%40globalscope.html#enum-globalscope-keylist

# terminology
# control refers to an input_event. simply a nicer word for it in this context
# action is the name of an action to which a control can be assigned. same meaning as in InputMap

# database of the default keybindings for each control scheme
# each control scheme contains a list of controls assigned to each action
var scheme_controls := {
	keyboard = {
		"left": key(KEY_A),
		"down": key(KEY_S),
		"up": key(KEY_W),
		"right": key(KEY_D),
		"action1": key(KEY_J),
		"action2": key(KEY_K),
		},
	arrows = {
		"left": key(KEY_LEFT),
		"down": key(KEY_DOWN),
		"up": key(KEY_UP),
		"right": key(KEY_RIGHT),
		"action1": key(KEY_X),
		"action2": key(KEY_Z),
		},
	colemak_left = {
		"left": key(KEY_R),
		"down": key(KEY_S),
		"up": key(KEY_F),
		"right": key(KEY_T),
		"action1": key(KEY_N),
		"action2": key(KEY_E),
		},
	colemak_right = {
		"left": key(KEY_R),
		"down": key(KEY_S),
		"up": key(KEY_F),
		"right": key(KEY_T),
		"action1": key(KEY_N),
		"action2": key(KEY_E),
		},
	gamepad = {
		"left": joy(JOY_DPAD_LEFT),
		"down": joy(JOY_DPAD_DOWN),
		"up": joy(JOY_DPAD_UP),
		"right": joy(JOY_DPAD_RIGHT),
		"action1": joy(JOY_SONY_CIRCLE),
		"action2": joy(JOY_SONY_X),
		},
	gamepad_analog = {
		"left": axis(JOY_AXIS_0, -1),
		"right": axis(JOY_AXIS_0, 1),
		"up": axis(JOY_AXIS_1, -1),
		"down": axis(JOY_AXIS_1, 1),
		},
	}

# mapping our actions to the ui_ actions as well
# e.g. if we define right as D, then we also want ui_right to respond to D
# not sure if ui_select is needed
# all the ui_ actions are meant to be used internally by godot control nodes, so you shouldn't delete them
# likely just leave the default binds untouched unless there's reason to change them
# and if there is, just change/remove them from the inputmap dialog in project settings
# this mapping here is simply to automatically map what we've already configured in the schemes
const actions_to_ui_map := {
	"left": "ui_left",
	"down": "ui_down",
	"up": "ui_up",
	"right": "ui_right",
#	"action1": "ui_accept",
#	"action2": "ui_cancel",
	}

# actions defines the actions we make use of in-game
# does not have to match the scheme controls list
var actions_enabled := [
	"left", "down", "up", "right", "action1", "action2",
	]
# control schemes defines which schemes we are using
#var schemes_enabled := [ "keyboard", "gamepad", "colemak_left" ]
var schemes_enabled := [ ]
#var control_schemes_visible := [ "keyboard", "gamepad", "gamepad_analog" ]

# these are quick and dirty additions
# they are not meant to be exposed to the user for rebinding
# and not meant to be saved in the config file
# simply a more portable way than using projectsettings inputmap
# think of it as a 1:1 replacement for that screen
# some of these are used in nframework code as well, so we might want to make a different list for them
var custom_actions := {
	"click": mouse(BUTTON_LEFT),
	"pause": key(KEY_P),
	"menu": key(KEY_ESCAPE),
	"1": key(KEY_1),
	}

# generated list of all controls assigned to each action
# dict[action][scheme] = control (as inputevent)
# each action can end up with more than one control (one for each scheme)
# schemes can be different devices but also just alternative controls (e.g. keyboard_alt)
var action_controls := {}

var actions_initialized := false

export(bool) var gamepad_swap: bool = false


# shortcuts
func key(scancode):
	return InputControl.new().new_key(scancode)
func mouse(button_index):
	return InputControl.new().new_mouse(button_index)
func joy(button_index):
	return InputControl.new().new_joy(button_index)
func axis(axis, axis_value):
	return InputControl.new().new_axis(axis, axis_value)


func export_custom_properties() -> Dictionary:
	var keybinds = {}
	for action in action_controls:
		keybinds[action] = {}
		for scheme in action_controls[action]:
			var control: InputControl = action_controls[action][scheme]
			keybinds[action][scheme] = control.serialize()

	return {"keybinds": keybinds}


func restore_custom_properties(props: Dictionary):
	var keybinds = props["keybinds"]
	for action in keybinds:
		action_controls[action] = {}
		for scheme in keybinds[action]:
			var input_event_data = keybinds[action][scheme]
			action_controls[action][scheme] = InputControl.new().from_serialized(input_event_data)


func init() -> void:
#	if Config.debug_build:
#		schemes_enabled = [ "keyboard", "gamepad", "colemak_left" ]
#	else:
#		schemes_enabled = [ "keyboard", "gamepad" ]

	# optionally clear all of godot's default actions
#	clear_all_actions()

	# InputMap is global so it persists everywhere

	# if we haven't loaded from config file, initialize defaults
#	if not action_controls:
#		for action in actions_enabled:
#			action_controls[action] = {}
#			for scheme in schemes_enabled:
#				action_controls[action][scheme] = create_control_from_db(action, scheme)
#
#	setup_inputmap_actions()
	setup_inputmap_actions_custom()

	actions_initialized = true
	emit_signal('actions_initialized')

#	print_default_ui_keys()
#	print_action_controls()


# setup InputMap actions for each entry in action_controls
func setup_inputmap_actions() -> void:
	for action in action_controls:
		InputMap.add_action(action)
		for scheme in action_controls[action]:
			var control = action_controls[action][scheme]
			replace_action(action, control, scheme)


func setup_inputmap_actions_custom() -> void:
	for action in custom_actions:
		InputMap.add_action(action)
		InputMap.action_add_event(action, custom_actions[action].input_event)


# this can also be used to add new actions
func replace_action(action, control, scheme) -> void:
	InputMap.action_erase_event(action, action_controls[action][scheme].input_event)
	InputMap.action_add_event(action, control.input_event)
	action_controls[action][scheme] = control

	if actions_to_ui_map.has(action):
		InputMap.action_erase_event(actions_to_ui_map[action], action_controls[action][scheme].input_event)
		InputMap.action_add_event(actions_to_ui_map[action], control.input_event)


func rebind_from_menu(action, event, scheme) -> InputControl:
	var control = InputControl.new().from_event(event)
	control.action = action
	control.scheme = scheme
	replace_action(action, control, scheme)
	return control


#func clear_all_actions() -> void:
#	var actions = InputMap.get_actions()
#	for a in actions:
#		InputMap.action_erase_events(a)


func create_control_from_db(action, scheme) -> InputControl:
	var scheme_dict = scheme_controls[scheme]

	# var event: InputEvent
	var control: InputControl
	if scheme_dict.has(action):
		# event = scheme_dict[action]
		control = scheme_dict[action]
		control.action = action
		control.scheme = scheme
	else:
		# event = InputEvent.new()
		control = InputControl.new()
		control.action = action
		control.scheme = scheme

	return control


func swap_gamepad(value):
	if not actions_initialized:
		yield(self, "actions_initialized")

	gamepad_swap = value
	# retro consoles: 1 is confirm, 0 is cancel (ps O, X)
	# western modern consoles: 0 is confirm, 1 is cancel (ps X, O)
#	# by default

	Log.d(["swapping gamepad buttons"])

	var button_0 = joy(JOY_BUTTON_0)
	var button_1 = joy(JOY_BUTTON_1)

	if value == true:
		# swap to modern
		replace_action("action1", button_0, "gamepad")
		replace_action("action2", button_1, "gamepad")
	else:
		# default to retro
		replace_action("action1", button_1, "gamepad")
		replace_action("action2", button_0, "gamepad")


func print_default_ui_keys() -> void:
	for a in InputMap.get_actions():
		# exclude/choose ui_ default mappings
		if a.left(3) == 'ui_':
			var log_str = str(a, ": ")
			for input_event in InputMap.get_action_list(a):
				log_str += get_inputevent_name(input_event)
#				if input_event is InputEventJoypadButton:
##					log_str += str("JoypadButton ", input_event.button_index)
#					log_str += get_inputevent_name(input_event)
#				else:
#					log_str += str(input_event.as_text())
				log_str +=  " | "
			log_str = log_str.trim_suffix(" | ")
			Log.d(log_str, "input_ui")


# type can be ui, scheme, custom, all
func print_inputmap_controls(filter: String = "all") -> void:
#func clear_all_actions() -> void:
	var actions = InputMap.get_actions()
	for action in actions:
		if filter == "ui" and ! action.begins_with("ui_"):
			continue
		if filter == "scheme" and ! actions_enabled.has(action):
			continue
		if filter == "custom" and ! custom_actions.has(action):
			continue
		if filter == "all":
			pass

		var controls = InputMap.get_action_list(action)
		var log_str = str(action, ": ")
		for input_event in controls:
			var ie_name = get_inputevent_name(input_event)
			if ie_name:
				log_str += ie_name
#				log_str += input_event.as_text()
			else:
				log_str += "[unbound]"

			log_str += " | "
		log_str = log_str.trim_suffix(" | ")
		Log.d(log_str, "input_map")


# this is only for presentation purposes. not for internal usage
static func get_inputevent_name(event: InputEvent) -> String:
	var text: String = ""
	if event is InputEventKey:
		text += event.as_text()
		# uncomment this line to show physical
		text = text.replace(" (Physical)", "")
	elif event is InputEventMouseButton:
		text += "Mouse"
		var as_text = event.as_text()
		var button_name = as_text.split(", ")[0].split(" : ")[1].split("=")[1]
		text += StringUtils.snake_to_pascal(button_name.to_lower())
	elif event is InputEventJoypadButton:
#		if Input.is_joy_known(event.device):
#			text+= str(Input.get_joy_button_string(event.button_index))
#		else:
#			text += "Btn. " + str(event.button_index)
		if event.button_index == JOY_INVALID_OPTION:
			text += "undefined"
		else:
			text += str(Input.get_joy_button_string(event.button_index))
	elif event is InputEventJoypadMotion:
		if event.axis == JOY_INVALID_OPTION:
			text += "undefined"
			return text

#		if Input.is_joy_known(event.device):
#			axis = str(Input.get_joy_axis_string(event.axis))
#			text += axis + " "
#		else:
#			text += "Axis: " + str(event.axis) + " "

		var axis := str(Input.get_joy_axis_string(event.axis))
		var axis_value: int = event.axis_value

		var direction := ""
		if axis.ends_with('X'):
			if axis_value > 0:
				direction = 'Right'
			else:
				direction = 'Left'
			text += axis.replace("X", direction)
		elif axis.ends_with('Y'):
			if axis_value > 0:
				direction = 'Down'
			else:
				direction = 'Up'
			text += axis.replace("Y", direction)
		else:
			text += axis + " " + str(axis_value)

	return text
