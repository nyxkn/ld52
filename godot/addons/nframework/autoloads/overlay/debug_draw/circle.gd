tool
extends Node2D
class_name Circle


export(int) var radius := 16 setget set_radius
export(Color) var color := Color.white setget set_color

export(bool) var border := false setget set_border
export(Color) var border_color := Color.black setget set_border_color
export(int, 1, 20) var border_width := 2 setget set_border_width


func _draw() -> void:
	draw_circle(Vector2.ZERO, radius, color)
	if border:
		draw_arc(Vector2.ZERO, radius - (border_width/2.0), 0.0, 2*PI, 128,
			border_color, border_width, true)


func set_radius(value):
	radius = value
	update()

func set_color(value):
	color = value
	update()

func set_border(value):
	border = value
	update()

func set_border_color(value):
	border_color = value
	update()

func set_border_width(value):
	border_width = value
	update()

