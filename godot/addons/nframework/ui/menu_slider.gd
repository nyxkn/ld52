tool
extends HBoxContainer


export(float) var min_value: float = 0
export(float) var max_value: float = 100
export(float) var step: float = 1


func _ready():
	if Engine.editor_hint:
		$Label.text = name

	$HSlider.min_value = min_value
	$HSlider.max_value = max_value
	$HSlider.step = step

	$Label.rect_min_size.x = int(min(64, F.screen_size.x / 10))
