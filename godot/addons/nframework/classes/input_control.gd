class_name InputControl


enum EventType { UNBOUND, KEY, MOUSE, JOY, AXIS }
var event_type: int = EventType.UNBOUND

# default to an empty inputeventkey. inputevent base class cannot be initialized
var input_event: InputEvent = InputEventKey.new()
var action: String = ""
var scheme: String = ""

var event_type_map := {
	EventType.KEY: InputEventKey.new().get_class(),
	EventType.MOUSE: InputEventMouseButton.new().get_class(),
	EventType.JOY: InputEventJoypadButton.new().get_class(),
	EventType.AXIS: InputEventJoypadMotion.new().get_class(),
	}


# public constructors
func new_key(scancode):
	return _new_event(EventType.KEY, scancode)

func new_mouse(button_index):
	return _new_event(EventType.MOUSE, button_index)

func new_joy(button_index):
	return _new_event(EventType.JOY, button_index)

func new_axis(axis, axis_value):
	return _new_event(EventType.AXIS, [axis, axis_value])

func from_event(input_event: InputEvent) -> InputControl:
	self.input_event = input_event
	self.event_type = Utils.dict_get_key_of_value(event_type_map, input_event.get_class())
	return self

func from_serialized(serialized):
	var event_type = EventType[serialized[0]]
	if event_type == EventType.UNBOUND:
		return self

	var event_data_save = serialized[1]

	match event_type:
		EventType.KEY:
			_new_event(event_type, OS.find_scancode_from_string(event_data_save))
		EventType.MOUSE:
			_new_event(event_type, event_data_save)
		EventType.JOY:
			_new_event(event_type, Input.get_joy_button_index_from_string(event_data_save))
		EventType.AXIS:
			var event_data = [
				Input.get_joy_axis_index_from_string(event_data_save[0]),
				event_data_save[1]
				]
			_new_event(event_type, event_data)

	return self


func serialize() -> Array:
	var event_data_save
	match event_type:
		EventType.KEY:
			event_data_save = OS.get_scancode_string(input_event.physical_scancode)
		EventType.MOUSE:
			event_data_save = input_event.button_index
		EventType.JOY:
			event_data_save = Input.get_joy_button_string(input_event.button_index)
		EventType.AXIS:
			var axis_name = Input.get_joy_axis_string(input_event.axis)
			var axis_value = input_event.axis_value
			event_data_save = [axis_name, axis_value]

	if event_type == EventType.UNBOUND:
		return ["UNBOUND"]
	else:
		return [EventType.keys()[event_type], event_data_save]


func get_inputevent_name():
	return SettingsControls.get_inputevent_name(input_event)


func _new_event(event_type: int, event_data) -> InputControl:
	match event_type:
		EventType.KEY:
			input_event = InputEventKey.new()
			input_event.physical_scancode = event_data
		EventType.MOUSE:
			input_event = InputEventMouseButton.new()
			input_event.button_index = event_data
		EventType.JOY:
			input_event = InputEventJoypadButton.new()
			input_event.button_index = event_data
		EventType.AXIS:
			input_event = InputEventJoypadMotion.new()
			input_event.axis = event_data[0]
			input_event.axis_value = event_data[1]

	self.event_type = event_type
	return self
