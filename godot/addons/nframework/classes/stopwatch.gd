class_name Stopwatch
extends Node

# this is gametime
# it's affected by engine slowdowns and pause

# WARNING: remember you have to add this as a child for _process to work

var time: float = -1
var paused: bool = false setget set_pause


func _init() -> void:
	name = "Stopwatch"
	pause_mode = Node.PAUSE_MODE_STOP
	set_process(false)


func _process(delta: float) -> void:
	time += delta


func start() -> void:
	if not is_inside_tree():
		Log.e("you need to add this to the scenetree before calling start", name)
		return

	if self.paused:
		Log.w("starting (and resetting) a paused stopwatch. was this intended?", name)
		paused = false # resetting the value without calling set_pause
	else:
		Log.d("starting (and resetting) stopwatch", name)

	time = 0.0
	set_process(true)


func stop() -> void:
#	time = -1 # don't reset time. we want to read it
	set_process(false)


func set_pause(value) -> void:
	paused = value
	set_process(!paused)
