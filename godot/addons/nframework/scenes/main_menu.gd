extends Control


onready var button_newgame: Button = $Control/MenuOptions/NewGame
onready var button_settings: Button = $Control/MenuOptions/Settings
onready var button_exit: Button = $Control/MenuOptions/Exit
onready var button_help: Button = $Control/MenuOptions/Help


func _ready():
#	theme = load(Config.theme)
#	$Control/Title.text = ProjectSettings.get_setting("application/config/name")

	if Config.html5:
		button_exit.visible = false

	button_newgame.connect("pressed", self, "_on_NewGame_pressed")
	button_settings.connect("pressed", self, "_on_Settings_pressed")
	button_exit.connect("pressed", self, "_on_Exit_pressed")
	button_help.connect("pressed", self, "_on_Help_pressed")

	setup_focus()

	if Score.best_score:
		$"%BestScore".value = Score.best_score.score
	else:
		$"%BestScore".value = "0"


func setup_focus():
	if Config.menu_control & Config.MenuControl.KEYBOARD:
		Utils.setup_focus_grabs_on_mouse_entered($Control/MenuOptions)
		Utils.grab_first_focus_recursive($"%MenuOptions")
#		button_newgame.grab_focus()


func _on_NewGame_pressed():
	# example for ensuring loading has finished before starting
	var loader = get_tree().root.get_node_or_null("PreLoader")
	if not loader or loader.finished:
		F.change_scene(Config.scenes.game, {}, 1.0)
	else:
		Log.e("preloader resources are still being loaded. we shouldn't let this happen", name)


func _on_Settings_pressed():
	F.change_scene(Config.scenes.settings_menu)


func _on_Help_pressed():
	F.change_scene(Config.scenes.help)


#func _on_Credits_pressed():
#    pass # Replace with function body.


func _on_Exit_pressed():
	get_tree().quit()


