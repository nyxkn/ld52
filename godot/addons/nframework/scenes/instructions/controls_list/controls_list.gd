extends Control

const ControlDescription = preload("control_description.tscn")

const actions_description := {
	up = 'up',
	left = 'left',
	down = 'down',
	right = 'right',
	action1 = 'attack',
	action2 = 'jump',
	}


func _ready() -> void:
	for action in actions_description:
		var controls = SettingsControls.action_controls[action]
		var control_description = ControlDescription.instance().init(controls['keyboard'], actions_description[action])
		$GridContainer.add_child(control_description)

