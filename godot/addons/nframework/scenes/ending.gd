extends Control


#var keypress_enabled: bool = false

onready var button_continue = $"%Continue"

var scene_change_data


func _ready() -> void:
#	pass

#	$Score.value = scene_change_data.score
#	$PlayTime.value = "%.1f" % scene_change_data.playtime
#	$BestScore.value = G.best_score

#	$Scores/ScoreValue.text = "%d" % scene_change_data.score
	if Score.last_score:
		$"%ScoreValue".text = "%d" % Score.last_score.score
		$"%BestScoreValue".text = "%d" % Score.best_score.score
#		$Scores/PlayTimeValue.text = "%.1fs" % scene_change_data.playtime

	if scene_change_data:
		var playtime = scene_change_data.stats.playtime
		playtime = TimeUtils.seconds_to_minutes(playtime)
#		$"%PlayTimeValue".text = "%.1fs" % playtime
		$"%PlayTimeValue".text = playtime.split(".")[0] + "m"
		$"%UpgradesValue".text = "%d / %d" % [
			scene_change_data.stats.upgrades_unlocked,
			scene_change_data.total_upgrades]
		$"%PlantsValue".text = "%d" % scene_change_data.stats.plants_harvested
		$"%ClicksValue".text = "%d" % scene_change_data.stats.mouse_clicks

#	yield(get_tree().create_timer(1.5), "timeout")
#	keypress_enabled = true
#	button_continue.grab_focus()

	G.music_player.goto_section("Intro", NDef.When.ODD_BAR)

	yield(get_tree().create_timer(1.0), "timeout")
	$Confetti.emitting = true

func _input(event) -> void:
	pass
#	if keypress_enabled:
	# make sure this is not a key player were spamming, or use the keypress enabled delay
#	if event is InputEventKey:
#		if event.scancode == KEY_ENTER:
#			_on_Continue_pressed()


func _on_Continue_pressed() -> void:
	F.change_scene(Config.scenes.game, {}, 0.25)


func _on_MainMenu_pressed() -> void:
	F.change_scene(Config.scenes.main_menu, {}, 0.25)
