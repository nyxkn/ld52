tool
extends VBoxContainer


func _on_User_pressed() -> void:
	OS.shell_open(ProjectSettings.globalize_path("user://"))


func _on_Res_pressed() -> void:
	OS.shell_open(ProjectSettings.globalize_path("res://"))
