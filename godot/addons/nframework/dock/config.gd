tool
extends MarginContainer


func _ready() -> void:
	populate_toggles()


func _on_SaveCfg_pressed() -> void:
	Config.save_cfg()


func _on_PrintValues_pressed() -> void:
	for prop in Utils.get_export_variables(self):
		print(str(prop.name, " = ", Config.get(prop.name)))


func _on_Test_pressed() -> void:
	pass
#	print(Config.editor_interface)


func _on_DeleteCfgs_pressed() -> void:
	FileUtils.delete_file(Config.cfg_save_path)
	FileUtils.delete_file(Config.settings_cfg_save_path)


func populate_toggles() -> void:
	var toggles = $"%Toggles"
	var ConfigToggle = preload("config_toggle.tscn")

	for prop in Utils.get_export_variables(Config):
		if prop.type == TYPE_BOOL:
			var new_toggle = ConfigToggle.instance()
			new_toggle.name = prop.name
			new_toggle.tracking_property = prop.name
			toggles.add_child(new_toggle)


