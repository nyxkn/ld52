# Game Title

*This is my Compo submission for [Ludum Dare 52](https://ldjam.com/events/ludum-dare/52/wheat-master). Theme: Harvest.*

*Play it on [itch.io](https://nyxkn.itch.io/wheat-master).*

## License

### Source code

All source code is licensed under the terms of the [GPL-3.0-only License](https://spdx.org/licenses/GPL-3.0-only.html).

### Assets

All assets (images and audio files) are licensed under the [CC-BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).

This includes everything in the *assets-source*, *media*, and *godot/assets* folders.
